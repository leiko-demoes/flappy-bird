//
// Created by Matty on 2.6.2020.
//

#include "Bird.h"
void Bird::Update()
{
    this->m_pPos += this->m_pVelocity;
    this->m_pVelocity *= 0.75f;
    this->m_pVelocity.y = (this->m_pVelocity.y-0.01f) < -MAX_VELOCITY ? -MAX_VELOCITY : this->m_pVelocity.y-0.01f ;
    m_collider.setPosition(m_pPos);
}
void Bird::Draw(const std::shared_ptr<Leiko::Render::Types::DrawBatch2D>& batch,
        const std::shared_ptr<Leiko::Render::Types::Texture>& birdTexture)
{
    float rotation = 0 - 3*(m_pVelocity.y/MAX_VELOCITY) * 90;
    Leiko::Render::Primitives2D::Quad(m_pPos,{0.1,0.17778},rotation,birdTexture).draw(batch);
}
void Bird::ApplyAcceleration(glm::vec2 acceleration)
{
    this->m_pVelocity.x = (acceleration.x + m_pVelocity.x) > MAX_VELOCITY ? MAX_VELOCITY : (acceleration.x + m_pVelocity.x);
    this->m_pVelocity.y = (acceleration.y + m_pVelocity.y) > MAX_VELOCITY ? MAX_VELOCITY : (acceleration.y + m_pVelocity.y);
}
