//
// Created by Matty on 2.6.2020.
//

#ifndef SANDBOX_OBSTACKLE_H
#define SANDBOX_OBSTACKLE_H

#include <leiko/Leiko.h>
#include <leiko/render/primitives/2D/Quad.h>
#include <leiko/lib/glm/vec2.hpp>
#include <leiko/render/types/DrawBatch.h>
#include <leiko/collision/colliders/2d/2DColliders.h>

class Obstackle {
private:
    glm::vec2 m_pPos;
    bool m_pIsCollected = false;
public:
    std::shared_ptr<Leiko::Collision::Collision2D::AABBCollider> m_topCollider;
    std::shared_ptr<Leiko::Collision::Collision2D::AABBCollider> m_bottomCollider;
    std::shared_ptr<Leiko::Collision::Collision2D::AABBCollider> m_collectionCollider;
    glm::vec2 getPos() {return m_pPos;};
    bool m_alive = false;
    Obstackle() {
        m_topCollider = std::make_shared<Leiko::Collision::Collision2D::AABBCollider>(glm::vec2(-0.1f,-0.5f),glm::vec2(0.1f,0.5f),glm::vec2(1.25f, 1.0));
        m_bottomCollider = std::make_shared<Leiko::Collision::Collision2D::AABBCollider>(glm::vec2(-0.1f,-0.5f),glm::vec2(0.1f,0.5f),glm::vec2(1.25f, -1.0));
        m_collectionCollider = std::make_shared<Leiko::Collision::Collision2D::AABBCollider>(glm::vec2(-0.09f,-0.5f),glm::vec2(0.09f,0.5f),glm::vec2(1.25f, 0.0));
    }
    void Draw(const std::shared_ptr<Leiko::Render::Types::DrawBatch2D> & batch,const std::shared_ptr<Leiko::Render::Types::Texture> & pipeTexture);
    void Update(float & highestX);
    float Respawn(float lastY);
    void Reset() {
        m_pPos = {-10.0f,0.0f};
    };
    bool isCollected() { return m_pIsCollected; }
    void collect() { m_pIsCollected = true; }
};

#endif //SANDBOX_OBSTACKLE_H
