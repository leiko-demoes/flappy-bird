//
// Created by mattyvrba on 6/30/20.
//

#ifndef LEIKO_GLOBAL_H
#define LEIKO_GLOBAL_H

namespace Leiko {
    class Global {
    public:
        /// If glad was already initialized
        static bool s_pGladInitialized;
    };
}

#endif //LEIKO_GLOBAL_H
