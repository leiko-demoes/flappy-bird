//
// Created by Matty on 13.6.2020.
//

#ifndef LEIKO_RENDERPRIMITIVE_H
#define LEIKO_RENDERPRIMITIVE_H

#include "../types/DrawBatch2D.h"
#include <memory>
#define GLM_ENABLE_EXPERIMENTAL
#define GLM_SWIZZLE
#include "../../lib/glm/glm.hpp"
#include "../../lib/glm/gtx/vec_swizzle.hpp"
#include "../../lib/glm/gtx/transform.hpp"
#include "../types/Texture.h"
#include "LeikoVertex.h"

namespace Leiko {
namespace Render {
class RenderPrimitive {
protected:
    /// Vertices
    std::vector<LeikoVertex> m_pVertexData;
    /// Base positions
    std::vector<glm::vec4> m_pBaseVertexPositions;
    /// Indices
    std::vector<uint32_t> m_pIndexData;
public:
    /// Draws data to batch
    /// \param batch Batch that data will be drawn to
    virtual void draw(const std::shared_ptr<Types::DrawBatch2D>& batch) = 0;
};
}
}

#endif //LEIKO_RENDERPRIMITIVE_H
