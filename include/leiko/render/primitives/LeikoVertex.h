//
// Created by Matty on 16.5.2020.
//

#ifndef LEIKO_LEIKOVERTEX_H
#define LEIKO_LEIKOVERTEX_H

#include "../types/Vertex.h"

namespace Leiko {
namespace Render {
class LeikoVertex : public Leiko::Render::Types::Vertex {
public:
    void operator=(const LeikoVertex& v);
    /// Creates leiko base vertex ( Basic layout is 3F pos, 4F color, 2F UV ,1F hasTexture, 4F textureTilingData (Size and Min) )
    /// \param pos
    /// \param color
    /// \param uv
    /// \param tilingFactor
    /// \param isTextured
    /// \param uvDATA Whole batch of uv data (used for saving data )
    LeikoVertex(glm::vec3 pos, glm::vec4 color,glm::vec2 uv, bool isTextured,glm::vec4 uvDATA);
    uint32_t getFloatCount() override;
    Types::VertexLayout getVertexLayout() override;
};
}
}

#endif //LEIKO_LEIKOVERTEX_H
