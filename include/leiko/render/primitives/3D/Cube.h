//
// Created by Matty on 13.6.2020.
//

#ifndef LEIKO_CUBE_H
#define LEIKO_CUBE_H

#include "../RenderPrimitive.h"

namespace Leiko{
namespace Render {
namespace Primitive3D {
class Cube : public Leiko::Render::RenderPrimitive {
public:
    void draw(const std::shared_ptr<Types::DrawBatch2D>& batch) override;
};
}
}
}
#endif //LEIKO_CUBE_H
