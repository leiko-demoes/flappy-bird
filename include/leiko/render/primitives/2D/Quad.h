//
// Created by Matty on 7.5.2020.
//

#ifndef LEIKO_QUAD_H
#define LEIKO_QUAD_H

#include "../RenderPrimitive.h"

namespace Leiko {
namespace Render {
namespace Primitives2D {
class Quad : public Leiko::Render::RenderPrimitive{
public:
    /// Creates quad object
    /// \param pos Position
    /// \param color Color
    /// \param size Size of quad
    /// \param rotation Rotation in radians
    /// \param texture Texture of quad
    /// \param tilingFactor tiling factor (1.0 means one texture)
    Quad(glm::vec3 pos, glm::vec4 color, glm::vec2 size, float rotation = 0,
            const std::shared_ptr<Types::Texture>& texture = nullptr, const glm::vec2& tilingFactor = glm::vec2(1.0));
    //! @copydoc Leiko::Render::Primitives2D::Quad::Quad(glm::vec3, glm::vec4, glm::vec2, float,const std::shared_ptr<Types::Texture>&,const glm::vec2&)
    Quad(glm::vec2 pos, glm::vec4 color, glm::vec2 size, float rotation = 0,
            const std::shared_ptr<Types::Texture>& texture = nullptr, const glm::vec2& tilingFactor = glm::vec2(1.0));
    /// Creates quad object
    /// \param pos Position
    /// \param size Size of quad
    /// \param rotation Rotation in radians
    /// \param texture Texture of quad
    /// \param tilingFactor tiling factor (1.0 means one texture)
    Quad(glm::vec2 pos, glm::vec2 size, float rotation = 0, const std::shared_ptr<Types::Texture>& texture = nullptr,
            const glm::vec2& tilingFactor = glm::vec2(1.0));
    void draw(const std::shared_ptr<Types::DrawBatch2D>& batch) override;
};
}
}
}

#endif //LEIKO_QUAD_H
