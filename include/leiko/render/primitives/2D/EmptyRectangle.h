//
// Created by Matty on 15.6.2020.
//

#ifndef LEIKO_EMPTYRECTANGLE_H
#define LEIKO_EMPTYRECTANGLE_H

#include "../RenderPrimitive.h"

namespace Leiko {
namespace Render {
namespace Primitives2D {
class EmptyRectangle : public Leiko::Render::RenderPrimitive {
public:
    /// Creates rectangle object that renders just as empty rectangle
    /// \param pos Position of rectangle
    /// \param color Color of rectangle
    /// \param size Size of rectangle
    /// \param rotation Rotation of rectangle in radians
    EmptyRectangle(glm::vec3 pos, glm::vec4 color, glm::vec2 size, float rotation = 0);
    //! @copydoc Leiko::Render::Primitives2D::EmptyRectangle::EmptyRectangle(glm::vec3, glm::vec4, glm::vec2, float)
    EmptyRectangle(glm::vec2 pos, glm::vec4 color, glm::vec2 size, float rotation = 0);
    void draw(const std::shared_ptr<Types::DrawBatch2D>& batch) override;
};
}}}
#endif //LEIKO_EMPTYRECTANGLE_H
