//
// Created by Matty on 10.4.2020.
//

#ifndef LEIKO_WINDOW_H
#define LEIKO_WINDOW_H

#include "base/Renderer.h"
#include <GLFW/glfw3.h>
#include <memory>
#include "../Global.h"

namespace Leiko {
namespace Render {

class Renderer;

/// Destructor for GLFW windows object
struct GLFWWindowDestructor {

  void operator()(GLFWwindow* ptr)
  {
      glfwDestroyWindow(ptr);
  }

};

class Window {
private:
    /// Background color
    glm::vec4 m_pBackgroundColor;
    /// Window name
    std::string m_pWindowName;
    /// GLFW Window object
    std::unique_ptr<GLFWwindow, Leiko::Render::GLFWWindowDestructor> m_pGLFWWindow;
    /// Renderer for selected window
    static std::unique_ptr<Renderer> m_pRenderer;
    /// Window dimensions
    static uint32_t m_pWindowWidth, m_pWindowHeight;
public:
    /// Gets window width
    /// \return Width of window
    static uint32_t GetWindowWidth() { return m_pWindowWidth; };
    /// Gets window height
    /// \return Height of window
    static uint32_t GetWindowHeight() { return m_pWindowHeight; };
    friend class WindowBuilder;
    /// Function called before rendering
    void preRender();
    /// Function called after rendering
    void finalizeRender();
    /// Gets renderer for this window
    /// \return Renderer reference
    std::unique_ptr<Renderer>& getRenderer() { return m_pRenderer; };
    Window() = default;
    /// Gets window name
    /// \return Window name
    const std::string& getWindowName();
    /// Returns GLFW window ptr
    /// \return GLFWWindow
    std::unique_ptr<GLFWwindow, Leiko::Render::GLFWWindowDestructor>& getGLFWWindow();
    /// Resize callback for window
    /// \param window Window object
    /// \param width new Width of window
    /// \param height new Height of window
    void FBSizeCallback(GLFWwindow* window, int32_t width, int32_t height);
};

class WindowBuilder : public std::enable_shared_from_this<WindowBuilder> {
public:
    WindowBuilder(uint32_t width, uint32_t height, std::unique_ptr<Renderer> renderer);
    std::shared_ptr<WindowBuilder> setWindowName(const std::string& name);
    std::shared_ptr<WindowBuilder> setBackgroundColor(const glm::vec4& bgColor);
    Leiko::Render::Window* finish();
private:
    bool m_pHasColor = false;
    glm::vec4 m_pBackgroundColor;
    std::unique_ptr<Renderer> m_pRenderer;
    bool m_pHasName = false;
    std::string m_pWindowName;
    bool m_pHasDecorations = false;
    uint32_t m_pWidth, m_pHeight = 0;
};

}
}
#endif //LEIKO_WINDOW_H
