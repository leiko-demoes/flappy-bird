//
// Created by Matty on 7.5.2020.
//

#ifndef LEIKO_DRAWBATCH_H
#define LEIKO_DRAWBATCH_H

#include <cstdint>
#include <vector>
#include "VertexBuffer.h"
#include "Vertex.h"
#include "Atlas.h"
#include "../base/Renderer.h"

namespace Leiko {
namespace Render {
namespace Types {
class Shader;

enum DrawType : uint32_t {
  POINTS = 0x0000,
  LINES = 0x0001,
  LINE_LOOP = 0x0002,
  LINE_STRIP = 0x0003,
  TRIANGLES = 0x0004,
  TRIANGLE_STRIP = 0x0005,
  TRIANGLE_FAN = 0x0006,
  QUADS = 0x0007
};

/// Drawbatch object used for drawing
class DrawBatch {
private:
    std::vector<float> m_pData;
    std::vector<uint32_t> m_pIndexData;
    uint32_t m_pIndicesIndex = 0;
    VertexBuffer m_pVertexBuffer;
    bool m_pHasTexture = false;
    std::shared_ptr<Leiko::Render::Types::Atlas> m_pAtlas;
    DrawType m_pDrawType = TRIANGLES;
public:
    ~DrawBatch() { }
    void flush();
    void finish();
    VertexBuffer& getVertexBuffer() { return m_pVertexBuffer; };
    DrawBatch(uint32_t bufferPerDraw = 2048);
    DrawBatch(const Leiko::Render::Types::DrawBatch& db);
    void addVertexData(Leiko::Render::Types::Vertex& vertex);
    /// Adds data to draw batch
    /// \param data
    void addData(float data);
    /// Adds data to draw batch
    /// \param data pointer to start of data
    /// \param size float count
    void addData(float* data, uint32_t size);
    /// Adds data to draw batch
    /// \param data
    void addData(std::vector<float>& data);

    void addIndex(uint32_t index);

    void addIndex(uint32_t* data, uint32_t size);

    void addIndex(std::vector<uint32_t>& data);

    void setAtlas(const std::shared_ptr<Leiko::Render::Types::Atlas>& atlas);

    void bindAtlasTextures();

    void setDrawType(DrawType type);

    DrawType getDrawType() { return m_pDrawType;}
};
}
}
}
#endif //LEIKO_DRAWBATCH_H
