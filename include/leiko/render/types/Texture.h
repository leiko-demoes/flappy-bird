//
// Created by Matty on 25.5.2020.
//

#ifndef LEIKO_TEXTURE_H
#define LEIKO_TEXTURE_H

#include <memory>
#include "../../lib/glm/glm.hpp"
#include "Atlas.h"


namespace Leiko {
namespace Render {
namespace Types {

class Atlas;

struct TextureRawData {
public:
    TextureRawData( uint32_t ID,uint32_t Width,uint32_t Height) :m_id(ID), m_width(Width), m_height(Height){}
    ~TextureRawData();
    /// Texture unit ID
    uint32_t m_id;
    /// Texture width and height
    uint32_t m_width, m_height;
};
class Texture {
private:
    /// UV in atlas
    glm::vec4 m_pAtlasUv;
public:
    /// Atlas in which is texture stored
    Leiko::Render::Types::Atlas* m_atlas = nullptr;
    friend class Atlas;
    glm::vec4 getUv(){  return m_pAtlasUv; };
};
}}}


#endif //LEIKO_TEXTURE_H
