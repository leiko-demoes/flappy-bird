//
// Created by Matty on 7.5.2020.
//

#ifndef LEIKO_VERTEXBUFFER_H
#define LEIKO_VERTEXBUFFER_H

#include <cstdint>
#include "../../Exceptions.h"

namespace Leiko {
namespace Render {
namespace Types {
class VertexBuffer {
private:
    uint32_t m_pGlVao, m_pGlVbo, m_pGlEbo;
    bool m_pHasSize = false;
    uint32_t m_pElementCount;
public:
    void bind();
    void setVertexData(uint32_t size, float* data, int32_t drawType);
    void setIndexData(uint32_t size, uint32_t* data, int32_t drawType);
    void setElementCount(uint32_t count)
    {
        m_pHasSize = true;
        m_pElementCount = count;
    }
    uint32_t getElementCount()
    {
        if (m_pHasSize) { return m_pElementCount; }
#ifndef NDEBUG
        else throw Leiko::Exceptions::LeikoException(std::string(__FILE__) + std::string(" : Line: ") + std::to_string(__LINE__)  +  std::string(" : Requested element count from VertexBuffer without size specified"),
                    "Vertex Buffer");
#else
        else throw Leiko::Exceptions::LeikoException(std::string("Requested element count from VertexBuffer without size specified"),
                    "Vertex Buffer");
#endif
    }
    VertexBuffer();
    ~VertexBuffer();
};
}
}
}
#endif //LEIKO_VERTEXBUFFER_H
