//
// Created by Matty on 14.6.2020.
//

#ifndef LEIKO_FRAMEBUFFER_H
#define LEIKO_FRAMEBUFFER_H

#include <cstdint>

class FrameBuffer {
private:
    uint32_t m_pID,m_pWidth,m_pHeight;
public:
    ~FrameBuffer();
    FrameBuffer(const FrameBuffer & other) = delete;
    FrameBuffer& operator=(const FrameBuffer & other) = delete;
    FrameBuffer ( FrameBuffer && other) noexcept;
    FrameBuffer(uint32_t width, uint32_t height);
};

#endif //LEIKO_FRAMEBUFFER_H
