//
// Created by Matty on 25.5.2020.
//

#ifndef LEIKO_ATLAS_H
#define LEIKO_ATLAS_H

#include <cstdint>
#include <memory>
#include "Vertex.h"
#include "Texture.h"
#include "Shader.h"

namespace Leiko {
namespace Render {
namespace Types {
class TextureRawData;
class Texture;

struct AtlasFreeSpace {
public:
    AtlasFreeSpace(float width, float height, float x, float y)
            :m_width(width), m_height(height), m_x(x), m_y(y) { };
    float m_width;
    float m_height;
    float m_x;
    float m_y;
};

class AtlasVertex : public Vertex {
public:
    AtlasVertex(glm::vec2 pos, glm::vec2 uv);
    uint32_t getFloatCount() override;
    VertexLayout getVertexLayout() override;
};

class Atlas{
private:
    static Leiko::Render::Types::Shader* s_atlasShader;
    static bool s_isAtlasShaderLoaded;

    void _loadAtlasShader();

    std::vector<std::pair<std::shared_ptr<Leiko::Render::Types::TextureRawData>,std::shared_ptr<Leiko::Render::Types::Texture>>> m_pTextures;

    uint32_t m_pTextureId;
    uint32_t m_pBloomTextureId;
    uint32_t m_pWidth, m_pHeight;
    uint32_t m_pFbo, m_pVbo, m_pVao, m_pEbo, m_pRbo;
public:
    static void init();
    static void deinit();
    uint32_t getTextureId(){ return m_pTextureId; };
    uint32_t getBloomTextureId(){ return m_pBloomTextureId; };
    Atlas(uint32_t sideSize);
    Atlas(uint32_t width, uint32_t height);
    ~Atlas();
    std::shared_ptr<Leiko::Render::Types::Texture> loadTexture(const std::string & path);
    std::shared_ptr<Leiko::Render::Types::Texture> loadTextureFromVram(uint32_t id, uint32_t width, uint32_t height);
    void finish();
    void bind();
};
}
}
}

#endif //LEIKO_ATLAS_H
