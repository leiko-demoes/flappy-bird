//
// Created by mattyvrba on 7/23/20.
//

#ifndef LEIKO_CAMERA_H
#define LEIKO_CAMERA_H

#include "../../../lib/glm/mat4x4.hpp"
#include "../../../lib/glm/vec3.hpp"
#include <utility>

namespace Leiko {
    namespace Render {
        namespace Types {
            typedef glm::mat4 ViewMatrix;
            typedef glm::mat4 ProjectionMatrix;

            /// Camera base object
            class Camera {
            protected:
                /// Position (X,Y,Z) where Y is up
                glm::vec3 m_pPosition;
                /// Rotation (Yaw,Pitch,Roll)
                glm::vec3 m_pRotation;
                /// Scale of camera
                glm::vec3 m_pScale;
            public:
                /// Sets position of camera
                /// \param position New position of camera
                void setPosition(const glm::vec3 &position);
                /// Sets rotation of camera
                /// \param rotation New rotation of camera
                void setRotation(const glm::vec3 &rotation);
                /// Sets scale of camera
                /// \param scale New scale of camera
                void setScale(const glm::vec3 &scale);
                /// Gets current position of camera
                /// \return Position of camera
                glm::vec3 getPosition();
                /// Gets current rotation of camera
                /// \return Rotation of camera
                glm::vec3 getRotation();
                /// Gets current scale of camera
                /// \return Scale of camera
                glm::vec3 getScale();
                /// Gets view matrix and projection matrix of camera
                /// \return View matrix and projection matrix
                virtual std::pair<ViewMatrix ,ProjectionMatrix> getViewMatrix() = 0;
            };
        }
    }
}

#endif //LEIKO_CAMERA_H
