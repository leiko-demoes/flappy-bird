//
// Created by mattyvrba on 7/27/20.
//

#ifndef LEIKO_PERSPECTIVECAMERA_H
#define LEIKO_PERSPECTIVECAMERA_H

#include "base/Camera.h"
#include "../../util/UtilMath.h"

namespace Leiko {
    namespace Render {
        namespace Types {
        class PerspectiveCamera : public Leiko::Render::Types::Camera {
        private:
            float m_pNear;
            float m_pFar;
        public:
            std::pair<Leiko::Render::Types::ViewMatrix, Leiko::Render::Types::ProjectionMatrix> getViewMatrix() override;
        };
        }
    }
}
#endif //LEIKO_PERSPECTIVECAMERA_H
