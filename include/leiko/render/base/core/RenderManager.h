//
// Created by Matty on 16.8.2020.
//

#ifndef LEIKO_RENDERMANAGER_H
#define LEIKO_RENDERMANAGER_H

#ifdef MINGW_WINDOWS_THREAD_LIB
#include <mingw_threads/mingw.thread.h>
#else
#include <thread>
#endif

class RenderManager {
private:
    bool m_pRenderThreadRun = true;
    std::unique_ptr<std::thread> m_pRenderThread;
    void _renderUpdate();
    void _renderInit();
public:
    void init();
    void stop();
};

#endif //LEIKO_RENDERMANAGER_H
