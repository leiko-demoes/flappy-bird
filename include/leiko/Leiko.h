#ifndef LEIKO_LEIKO_H
#define LEIKO_LEIKO_H

// Core includes
#define CUTE_C2_IMPLEMENTATION
#define GLM_SWIZZLE_XYZW
#include "lib/glm/glm.hpp"
#include "Game.h"
#include "render/base/Renderer.h"
#include "Exceptions.h"

// Types
#include "render/types/Shader.h"

// --- Entry point ---
#include "EntryPoint.h"
// ---             ---

#endif //LEIKO_LEIKO_H
