//
// Created by mattyvrba on 8/4/20.
//

#ifndef LEIKO_UTILTHREAD_H
#define LEIKO_UTILTHREAD_H

#include <string>
#include <atomic>
#include <utility>

namespace Leiko {
    namespace Util {
        /// Struct holding data for each thread, used to determine of thread should run and to hold its name
        struct ThreadData {
        private:
            /// Name of thread
            std::string m_pName;
            /// If thread should run
            std::atomic_bool m_pShouldRun;
        public:
            /// Constructor for thread data object, holding name and thread safe bool if thread should run
            /// \param name Name of thread this data object belongs to
            ThreadData(std::string name);
            /// Gets if current thread should continue to work
            /// \return if thread should run
            bool getShouldRun();
            /// Sets internal value of should run to false, eventually stopping thread
            void stop();
        };
        //todo: implement
        class ThreadSafeQueue {

        };

        class UtilThread {

        };
    }
}

#endif //LEIKO_UTILTHREAD_H
