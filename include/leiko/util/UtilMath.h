//
// Created by mattyvrba on 7/24/20.
//

#ifndef LEIKO_UTILMATH_H
#define LEIKO_UTILMATH_H

#include "../lib/glm/vec3.hpp"
#include "../lib/glm/glm.hpp"

namespace Leiko {
    namespace Util {
        /// Class holding math utility functions
        class UtilMath {
        public:
            /// Calculates normalized forward vector from given rotation (does not implement roll)
            /// \param rotationVector Vector holding rotation in order YAW, PITCH, ROLL
            /// \return normalized forward vector for given rotation
            static glm::vec3 getForwardFromRotation(const glm::vec3 & rotationVector) { // YAW, PITCH, ROLL
                auto direction = glm::vec3();
                direction.x = cos(glm::radians(rotationVector.x)) * cos(glm::radians(rotationVector.y));
                direction.y = sin(glm::radians(rotationVector.y));
                direction.z = sin(glm::radians(rotationVector.x)) * cos(glm::radians(rotationVector.y));
                return direction;
            }
        };
    }
}
#endif //LEIKO_UTILMATH_H
