//
// Created by Matty on 10.4.2020.
//

#ifndef LEIKO_GAME_H
#define LEIKO_GAME_H

#include <cstdint>
#include <memory>
#include "render/base/Renderer.h"
#include "render/Window.h"
#include <GLFW/glfw3.h>

namespace Leiko {
    namespace Render {
        namespace Types {
            class Shader;
        }
    }


    class Game {
    protected:
        std::unique_ptr<Render::Window> m_pWindow;
    private:
        /// Tick rate of updates per second
        uint32_t m_pTickRate = 32;
        /// Internal initialization function used for engines internal initialization
        void internalInit();
        /// Internal rendering function called before every render ( will be soon replaced with MTTR )
        void internalRender();
        /// Internal logic update function, called before every user defined update
        void internalUpdate();
        /// Internal rendering function that flushes data to screen at the end of rendering logic
        void finalizeRender();
        /// Time elapsed since last update tick happened, this is used for saving cpu time
        double m_pElapsedSinceFrame = 0.0;
        /// Time elapsed since last fps calculation
        double m_pElapsedSinceFpsUpdate = 0.0;
        /// FPS calculated last time
        uint32_t m_pFps = 0;
        /// FPS counted since last FPS calculation
        uint32_t m_pCountedFps = 0;
    public:
        /// Starts engine
        void start();
        /// User defined initialization function for initializing user data
        virtual void init() = 0;
        /// User defined update function, called every game tick
        virtual void update() = 0;
        /// User defined render function, called when rendering ( will be soon replaced with MTTR )
        virtual void render() = 0;
        /// Gets current frames per second, calculated every few seconds
        /// \return FPS
        uint32_t getFps() { return m_pFps; };
        /// Default leiko shader used by basic rendering
        std::shared_ptr<Leiko::Render::Types::Shader> m_leikoShader;
    };

    /// Function returning used defined object inheriting from game class
    /// \return User game
    Game* getGame();
}

#endif //LEIKO_GAME_H
