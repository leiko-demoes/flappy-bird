//
// Created by Matty on 10.6.2020.
//

#ifndef LEIKO_COLLIDER_H
#define LEIKO_COLLIDER_H

#include <memory>
#include "../../lib/glm/glm.hpp"

namespace Leiko {
namespace Collision {
struct CollisionManifold {
  uint32_t m_collisionCount;
  glm::vec2 m_collisionLocation[2];
  glm::vec2 m_collisionNormalFromAtoB;
};

class Collider {
public:
    /// Check if object collides with other collider
    /// \param other Other collider to check collision with
    /// \return if objects collide
    virtual bool collides(std::shared_ptr<Collider> other) = 0;
    //virtual CollisionManifold resolveCollision(std::shared_ptr<Collider> other) = 0;
};
}
}

#endif //LEIKO_COLLIDER_H
