//
// Created by Matty on 14.6.2020.
//

#ifndef LEIKO_CIRCLECOLLIDER_H
#define LEIKO_CIRCLECOLLIDER_H

#include "Collider2D.h"
#include "2DColliders.h"


namespace Leiko {
namespace Collision {
namespace Collision2D {
class CircleCollider : public Leiko::Collision::Collision2D::Collider2D {
private:
    glm::vec2 m_pPos;
    float m_pRadius;
public:
    c2Circle buildC2Circle();
    CircleCollider(const glm::vec2 & pos, float radius);
    bool collides(std::shared_ptr<Collider> other) override;
    /// Changes position of circle collider
    /// \param pos Position where to set collider
    void setPosition(const glm::vec2 & pos);
    /// Sets radius of circle collider
    /// \param radius new radius
    void setRadius(float radius);
    /// Returns radius of circle collider
    /// \return Radius
    float getRadius() { return m_pRadius; }
    /// Returns position of circle collider
    /// \return Position
    glm::vec2 getPosition() { return m_pPos;}
};
}}}

#endif //LEIKO_CIRCLECOLLIDER_H
