//
// Created by Matty on 14.6.2020.
//

#ifndef LEIKO_COLLIDER2D_H
#define LEIKO_COLLIDER2D_H

#include "../Collider.h"
#include "../../../lib/cute_c2.h"
#include "../../../Exceptions.h"

namespace Leiko {
namespace Collision {
namespace Collision2D {
enum ColliderType2D {
  CIRCLE_2D,
  CAPSULE_2D,
  AABB_2D,
  RAY_2D
};

class Collider2D : public Leiko::Collision::Collider {
protected:
    ColliderType2D m_pType;
    explicit Collider2D(ColliderType2D type) {m_pType = type;};
public:
    ColliderType2D getType() { return m_pType;}
};
}
}
}
#endif //LEIKO_COLLIDER2D_H
