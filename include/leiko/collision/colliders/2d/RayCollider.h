//
// Created by Matty on 14.6.2020.
//

#ifndef LEIKO_RAYCOLLIDER_H
#define LEIKO_RAYCOLLIDER_H

#include "Collider2D.h"
#include "2DColliders.h"

namespace Leiko {
namespace Collision {
namespace Collision2D {
class RayCollider : public Leiko::Collision::Collision2D::Collider2D {
private:
    glm::vec2 m_pPos;
    glm::vec2 m_pDirection;
    float m_pDistance;
public:
    RayCollider(glm::vec2 pos, glm::vec2 direction, float distance);
    c2Ray buildC2Ray();
    bool collides(std::shared_ptr<Collider> other) override;
};
}}}

#endif //LEIKO_RAYCOLLIDER_H
