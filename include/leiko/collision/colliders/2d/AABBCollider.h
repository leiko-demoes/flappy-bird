//
// Created by Matty on 14.6.2020.
//

#ifndef LEIKO_AABBCOLLIDER_H
#define LEIKO_AABBCOLLIDER_H

#include "Collider2D.h"
#include "2DColliders.h"

namespace Leiko {
namespace Collision {
namespace Collision2D {
class AABBCollider : public Leiko::Collision::Collision2D::Collider2D {
private:
    glm::vec2 m_pMin, m_pMax, m_pPos;
public:
    AABBCollider(const  glm::vec2 &min,const  glm::vec2 &max,const glm::vec2 &pos);
    c2AABB buildC2AABB();
    bool collides(std::shared_ptr<Collider> other) override;
    /// Changes position of aabb collider (Center of collider is where position will set to)
    /// \param pos Position where to set collider
    void setPosition(const glm::vec2 & pos);
    /// Returns position of collider
    /// \return Collider position
    glm::vec2 getPosition();
    /// Sets size for collider
    /// \param min Lower left corner of AABB collider
    /// \param max Higher right corner of AABB collider
    void setSize(const glm::vec2 & min,const glm::vec2 & max);
    /// Gets current size of collider
    /// \return Collider size
    glm::vec4 getSize() {return glm::vec4(m_pMin, m_pMax);}
};
}}}

#endif //LEIKO_AABBCOLLIDER_H
