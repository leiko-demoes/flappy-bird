//
// Created by Matty on 25.5.2020.
//

#ifndef LEIKO_LOG_H
#define LEIKO_LOG_H

#include <string>
#ifdef LINUX_BUILD
#else
#include <codecvt>
#include <locale>
#include <windows.h>
#undef ERROR
#endif



namespace Leiko {
class Log {
public:
    /// Shows crash popup and stops engine
    /// \param errorMessage Text shown in crash popup
    static void ShowCrashPopup(const std::string & errorMessage);
};
}
#endif //LEIKO_LOG_H
