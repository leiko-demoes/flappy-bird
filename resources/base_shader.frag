#version 330 core
out vec4 FragColor;

in vec4 vOut_color;
in vec2 vOut_uv;
in float vOut_isTextured;

uniform sampler2D u_texture;

void main()
{
    if(vOut_isTextured > 0.5f) {
        color = texture(u_texture,vOut_uv);
        color *= vOut_color;
    } else {
        color = vOut_color;
    }

    FragColor = color;
}
