//
// Created by Matty on 2.5.2020.
//

#ifndef SANDBOX_SANDBOX_H
#define SANDBOX_SANDBOX_H

#define MAX_SCREEN_OBSTACKLES 6

#include <leiko/Leiko.h>
#include <leiko/render/primitives/2D/Quad.h>
#include <leiko/render/primitives/2D/EmptyRectangle.h>
#include <leiko/render/types/Atlas.h>
#include "Obstackle.h"
#include "Bird.h"
#include <chrono>

class Sandbox : public Leiko::Game {
private:
    std::unique_ptr<Leiko::Render::Types::Shader> m_pShader;
    std::shared_ptr<Leiko::Render::Types::DrawBatch2D> m_pBackgroundBatch;
    std::shared_ptr<Leiko::Render::Types::DrawBatch2D> m_pTerrainBatch;
    std::shared_ptr<Leiko::Render::Types::DrawBatch2D> m_pBirdBatch;
    std::shared_ptr<Leiko::Render::Types::DrawBatch2D> m_pLoseBatch;
    std::shared_ptr<Leiko::Render::Types::DrawBatch2D> m_pColliderBatch;
    std::shared_ptr<Leiko::Render::Types::DrawBatch2D> m_pScoreBatch;
    std::shared_ptr<Leiko::Render::Types::Atlas> m_pAtlas;
    std::shared_ptr<Leiko::Render::Types::Texture> m_pBirdTexture;
    std::shared_ptr<Leiko::Render::Types::Texture> m_pPipeTexture;
    std::shared_ptr<Leiko::Render::Types::Texture> m_pBackgroundTexture;
    std::shared_ptr<Leiko::Render::Types::Texture> m_pLoseTextTexture;
    std::array<Obstackle,MAX_SCREEN_OBSTACKLES> m_pObstackles;
    std::array<std::shared_ptr<Leiko::Render::Types::Texture>,10> m_pNumberTextures;
    bool m_pFinished = false;

    std::shared_ptr<Bird> m_pBird;

    bool m_pLBwasPressed = false;
    float m_pLastY = 0.0f;
    uint32_t m_pScore = 0;

    std::chrono::time_point<std::chrono::system_clock> m_pLastFPS;
    uint32_t m_pFPSCustom = 0;
public:
    void init() override;
    void update() override;
    void render() override;
    void reset();
};

#endif //SANDBOX_SANDBOX_H
