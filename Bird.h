//
// Created by Matty on 2.6.2020.
//

#ifndef FLAPPYTEST_BIRD_H
#define FLAPPYTEST_BIRD_H

#include <leiko/Leiko.h>
#include <leiko/render/primitives/2D/Quad.h>
#include <leiko/lib/glm/vec2.hpp>
#include <leiko/render/types/DrawBatch.h>
#include <leiko/collision/colliders/2d/2DColliders.h>

#define MAX_VELOCITY 0.5f

class Bird {
private:
    glm::vec2 m_pPos = {0.0,0.0};
    glm::vec2 m_pVelocity={0.0,0.0};
public:
    Leiko::Collision::Collision2D::CircleCollider m_collider = Leiko::Collision::Collision2D::CircleCollider({0.0f,0.0f},0.05f);
    void Reset() {m_pPos = {0.0,0.0}; m_pVelocity={0.0,0.0};}
    void Draw(const std::shared_ptr<Leiko::Render::Types::DrawBatch2D> & batch,const std::shared_ptr<Leiko::Render::Types::Texture> & birdTexture);
    void Update();
    void ApplyAcceleration(glm::vec2 acceleration);
    glm::vec2 getPos() {return m_pPos;}
};

#endif //FLAPPYTEST_BIRD_H
