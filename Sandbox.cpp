//
// Created by Matty on 2.5.2020.
//

#include <iostream>
#include "Sandbox.h"

void Sandbox::init()
{
    m_pLastFPS = std::chrono::system_clock::now();
    auto windowBuilder = std::make_shared<Leiko::Render::WindowBuilder>(1920, 1080,
            std::make_unique<Leiko::Render::Renderer>())->setWindowName("Sandbox")
            ->setBackgroundColor({0.0f, 0.0f, 0.0f, 0.0f});

    auto shaderBuilder = std::make_shared<Leiko::Render::Types::ShaderBuilder>("resources/base_shader.vert",
            "resources/base_shader.frag");

    m_pWindow = std::unique_ptr<Leiko::Render::Window>(windowBuilder->finish());

    m_pAtlas = std::make_shared<Leiko::Render::Types::Atlas>(4500, 2048);

    m_pBackgroundTexture = m_pAtlas->loadTexture("resources/background.png");
    m_pPipeTexture = m_pAtlas->loadTexture("resources/pipe.png");
    m_pBirdTexture = m_pAtlas->loadTexture("resources/flappybird.png");
    m_pLoseTextTexture = m_pAtlas->loadTexture("resources/lose_text.png");

    for (int32_t i = 0; i<10; i++) {
        auto path = std::string("resources/"+std::to_string(i)+".png");
        m_pNumberTextures.at(i) = m_pAtlas->loadTexture(path);
    }

    m_pAtlas->finish();

    m_pShader = std::unique_ptr<Leiko::Render::Types::Shader>(shaderBuilder->finish());

    m_pLoseBatch = std::make_shared<Leiko::Render::Types::DrawBatch2D>(64);
    m_pLoseBatch->setAtlas(m_pAtlas);
    m_pBirdBatch = std::make_shared<Leiko::Render::Types::DrawBatch2D>(64);
    m_pBirdBatch->setAtlas(m_pAtlas);
    m_pBackgroundBatch = std::make_shared<Leiko::Render::Types::DrawBatch2D>(64);
    m_pBackgroundBatch->setAtlas(m_pAtlas);
    m_pColliderBatch = std::make_shared<Leiko::Render::Types::DrawBatch2D>(64);
    m_pScoreBatch= std::make_shared<Leiko::Render::Types::DrawBatch2D>(64);
    m_pTerrainBatch = std::make_shared<Leiko::Render::Types::DrawBatch2D>(200);
    m_pTerrainBatch->setAtlas(m_pAtlas);

    m_pBird = std::make_shared<Bird>();

    Leiko::Render::Primitives2D::Quad(glm::vec2(0.0), glm::vec2(2.0f), 0, m_pBackgroundTexture)
            .draw(m_pBackgroundBatch);
    m_pBackgroundBatch->finish();
    Leiko::Render::Primitives2D::Quad(glm::vec2(0.0), glm::vec2(1.5f, 0.8889f), 0, m_pLoseTextTexture)
            .draw(m_pLoseBatch);
    m_pLoseBatch->finish();

    m_pWindow->getRenderer()->setLineWidth(4);
}
void Sandbox::update()
{
    auto lbPressed = glfwGetMouseButton(m_pWindow->getGLFWWindow().get(), GLFW_MOUSE_BUTTON_1)==GLFW_PRESS;
    bool newLBPress = lbPressed && !m_pLBwasPressed;
    m_pLBwasPressed = lbPressed;

    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

    if (std::chrono::duration_cast<std::chrono::milliseconds>(now-m_pLastFPS).count()>1000) {
        m_pLastFPS = now;
        printf("FPS: %u\n", m_pFPSCustom);
        m_pFPSCustom = 0;
    }
    if (!m_pFinished) {
        auto highestX = -1.0f;

        for (auto& o : m_pObstackles) {
            o.Update(highestX);
        }

        if (highestX<(1.0f-2.0f/MAX_SCREEN_OBSTACKLES)) {
            for (auto& o : m_pObstackles) {
                if (!o.m_alive) {
                    m_pLastY = o.Respawn(m_pLastY);
                    break;
                }
            }
        }

        if (newLBPress) {
            m_pBird->ApplyAcceleration({0.0, 0.15});
        }

        m_pBird->Update();

        for (auto& a : m_pObstackles) {
            if (a.m_alive) {
                if(!a.isCollected() && m_pBird->m_collider.collides(a.m_collectionCollider)) {
                    m_pScore++;
                    a.collect();
                }

                if (m_pBird->m_collider.collides(a.m_topCollider) || m_pBird->m_collider.collides(a.m_bottomCollider)) {
                    m_pFinished = true;
                    break;
                }
            }
        }
    }

    if (glfwGetKey(m_pWindow->getGLFWWindow().get(), GLFW_KEY_R)==GLFW_PRESS) {
        reset();
    }
}
void Sandbox::render()
{
    m_pFPSCustom++;
    m_pTerrainBatch->flush();
    m_pBirdBatch->flush();
    m_pColliderBatch->flush();
    m_pScoreBatch->flush();
    m_pShader->activate();
    m_pShader->setUniformFloat("u_time", (float) glfwGetTime());
    m_pShader->setUniformInt("u_atlasTexture", 0);
    m_pShader->setUniformInt("u_atlasBloomTexture", 1);
    if (!m_pFinished) {
        for (auto& a : m_pObstackles) {
            m_pTerrainBatch->getVertexBuffer().bind();
            if (a.m_alive) a.Draw(m_pTerrainBatch, m_pPipeTexture);
            m_pColliderBatch->getVertexBuffer().bind();
            auto upperQuad = Leiko::Render::Primitives2D::EmptyRectangle(a.m_topCollider->getPosition(),
                    glm::vec4(1.0, 0.0, 1.0, 1.0), glm::vec2(glm::zw(a.m_topCollider->getSize()*2.0f)), 0.0f);
            auto lowerQuad = Leiko::Render::Primitives2D::EmptyRectangle(a.m_bottomCollider->getPosition(),
                    glm::vec4(1.0, 0.0, 1.0, 1.0), glm::vec2(glm::zw(a.m_topCollider->getSize()*2.0f)), 0.0f);
            auto scoreQuad = Leiko::Render::Primitives2D::EmptyRectangle(a.m_collectionCollider->getPosition(),
                    glm::vec4(1.0, 1.0, 0.0, 1.0), glm::vec2(glm::zw(a.m_collectionCollider->getSize()*2.0f)), 0.0f);
            upperQuad.draw(m_pColliderBatch);
            lowerQuad.draw(m_pColliderBatch);
            scoreQuad.draw(m_pColliderBatch);
        }
        m_pTerrainBatch->getVertexBuffer().bind();
        m_pTerrainBatch->finish();

        m_pBirdBatch->getVertexBuffer().bind();
        m_pBird->Draw(m_pBirdBatch, m_pBirdTexture);
        m_pBirdBatch->finish();

        m_pColliderBatch->getVertexBuffer().bind();
        auto colliderQuad = Leiko::Render::Primitives2D::EmptyRectangle(m_pBird->m_collider.getPosition(),
                glm::vec4(1.0, 0.0, 0.0, 1.0), glm::vec2(m_pBird->m_collider.getRadius()), 0.0f);

        colliderQuad.draw(m_pColliderBatch);
        m_pColliderBatch->finish();
    }
    m_pScoreBatch->getVertexBuffer().bind();
    auto scoreString = std::to_string(m_pScore);
    int32_t index = 0;
    for(auto & character : scoreString) {
        float xStart = 0.0f - scoreString.length() * 0.045f;
        float yLine = 0.5f;
        float width = 0.09f;
        float height = 0.18f;
        int value = character - '0';
        float center = xStart + index*width + width / 2.0f;
        auto characterQuad = Leiko::Render::Primitives2D::Quad(glm::vec2(center,yLine + height /2),glm::vec2(width,height),0,m_pNumberTextures.at(value));
        characterQuad.draw(m_pScoreBatch);
        index++;
    }
    m_pScoreBatch->finish();

    auto testVertex = Leiko::Render::LeikoVertex({}, {0.149, 0.713, 0.949, 0.0}, {0.0f, 0.0f}, false, {});

    m_pBackgroundBatch->getVertexBuffer().bind();
    m_pAtlas->bind();
    m_pWindow->getRenderer()->drawBuffer(*m_pBackgroundBatch, testVertex.getVertexLayout());

    if (!m_pFinished) {
        m_pTerrainBatch->getVertexBuffer().bind();
        m_pAtlas->bind();
        m_pWindow->getRenderer()->drawBuffer(*m_pTerrainBatch, testVertex.getVertexLayout());

        m_pBirdBatch->getVertexBuffer().bind();
        m_pAtlas->bind();
        m_pWindow->getRenderer()->drawBuffer(*m_pBirdBatch, testVertex.getVertexLayout());

        m_pColliderBatch->getVertexBuffer().bind();
        m_pWindow->getRenderer()->drawBuffer(*m_pColliderBatch, testVertex.getVertexLayout());
    }
    else {
        m_pLoseBatch->getVertexBuffer().bind();
        m_pAtlas->bind();
        m_pWindow->getRenderer()->drawBuffer(*m_pLoseBatch, testVertex.getVertexLayout());
    }

    m_pScoreBatch->getVertexBuffer().bind();
    m_pAtlas->bind();
    m_pWindow->getRenderer()->drawBuffer(*m_pScoreBatch, testVertex.getVertexLayout());
}
void Sandbox::reset()
{
    m_pBird->Reset();
    for (auto& a : m_pObstackles) {
        a.Reset();
    }
    m_pLastY = 0.0f;
    m_pScore = 0;
    m_pFinished = false;
}

Leiko::Game* Leiko::getGame()
{
    return new Sandbox();
}
