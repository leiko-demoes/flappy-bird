//
// Created by Matty on 2.6.2020.
//

#include "Obstackle.h"
void Obstackle::Draw(const std::shared_ptr<Leiko::Render::Types::DrawBatch2D>& batch,
        const std::shared_ptr<Leiko::Render::Types::Texture>& pipeTexture)
{
    auto topPos = glm::vec2(m_pPos.x, m_pPos.y+1.0);
    auto botPos = glm::vec2(m_pPos.x, m_pPos.y-1.0);
    Leiko::Render::Primitives2D::Quad(topPos, {0.5, 1.3}, 180, pipeTexture).draw(batch);
    Leiko::Render::Primitives2D::Quad(botPos, {0.5, 1.3}, 0, pipeTexture).draw(batch);
}
void Obstackle::Update(float& highestX)
{
    if (m_alive) {
        m_pPos.x -= 0.01f;
        if (m_pPos.x<-1.25f) {
            m_alive = false;
        }
        if (m_pPos.x>highestX) {
            highestX = m_pPos.x;
        }
        auto topPos = glm::vec2(m_pPos.x, m_pPos.y+1.0);
        auto botPos = glm::vec2(m_pPos.x, m_pPos.y-1.0);
        m_collectionCollider->setPosition(m_pPos);
        m_bottomCollider->setPosition(botPos);
        m_topCollider->setPosition(topPos);
    }
}
float Obstackle::Respawn(float lastY)
{
    float lowest = (lastY-0.3f)>-0.8f ? lastY-0.3f : -0.8f;
    float highest = (lastY+0.3f)<0.8f ? lastY+0.3f : 0.8f;

    float nextY = lowest+static_cast <float> (rand())/(static_cast <float> (RAND_MAX/(highest-lowest)));

    m_alive = true;
    m_pPos = {1.25f, nextY};
    m_pIsCollected = false;

    auto topPos = glm::vec2(m_pPos.x, m_pPos.y+1.0);
    auto botPos = glm::vec2(m_pPos.x, m_pPos.y-1.0);
    m_bottomCollider->setPosition(botPos);
    m_topCollider->setPosition(topPos);
    m_collectionCollider->setPosition(m_pPos);

    return nextY;
}
